<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com) (original implementation)
 * @copyright  Copyright (c) 2008 Fooman (http://www.fooman.co.nz) (use of Minify Library)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html page block
 *
 * @package  Fooman_Speedster
 * @author   Fooman (http://www.fooman.co.nz)
 */

class Fooman_Speedster_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{
    const CFG_ROOT  =   "dev/foo/";
    const MERGE     =   "merge";
    const MINIFY    =   "minify";
    const NOCHUNKB4 =   "noChunkB4";
    const NOCHUNK   =   "noChunk";
    const EXCSS     =   "exCss";
    const EXJS      =   "exJs";
    const EXAFTERJS =   "exAfterJs";

    /* Added ability to exlude from minifying and from chunking */

    public function getCssJsHtml()
    {
        //if(Mage::getStoreConfig( self::CFG_ROOT.self::MINIFY)){}
        //implement way to turn off minification and still have merging

        $webroot="/";

        $lines = array();

        $secure = Mage::app()->getStore()->isCurrentlySecure();

        $baseJs = Mage::getBaseUrl('js');

            if ($secure) {
                $baseJsFast = str_replace('index.php/','',Mage::getUrl('skin/m/',array('_secure'=>true)));
                //echo 'Secured';
            }
            else {
                $baseJsFast = Mage::getBaseUrl('skin').'m/';
                //echo 'Not Secured';
            }



        $html = '';

        $script = '<script type="text/javascript" src="%s" %s></script>';
        $stylesheet = '<link type="text/css" rel="stylesheet" href="%s" %s />';
        $alternate = '<link rel="alternate" type="%s" href="%s" %s />';

        foreach ($this->_data['items'] as $item) {
            if (!is_null($item['cond']) && !$this->getData($item['cond'])) {
                continue;
            }
            $if = !empty($item['if']) ? $item['if'] : '';
            switch ($item['type']) {
                case 'js':
                    if(stripos($item['name'], "http")===false){
                        if(strpos($item['name'], 'packaging.js') !==false) {
                            $item['name'] = $baseJs.$item['name'];
                            $lines[$if]['script_direct'][] = $item;
                        } else {
                            $lines[$if]['script']['global'][] = "/".$webroot."js/".$item['name'];
                        }
                    }
                    else{
                        $lines[$if]['script']['global'][] = $item['name']; }
                    break;

                case 'script_direct':
                    $lines[$if]['script_direct'][] = $item;
                    break;

                case 'css_direct':
                    $lines[$if]['css_direct'][] = $item;
                    break;

                case 'js_css':
                    $lines[$if]['other'][] = sprintf($stylesheet, $baseJs.$item['name'], $item['params']);
                    break;

                case 'skin_js':
                    $chunks=explode('/skin', $this->getSkinUrl($item['name']),2);
                    $lines[$if]['script']['skin'][] = "/".$webroot."skin".$chunks[1];
                    break;

                case 'skin_css':
                    if($item['params']== 'media="all"'){
                        $chunks=explode('/skin', $this->getSkinUrl($item['name']),2);
                        $lines[$if]['stylesheet'][] = "/".$webroot."skin".$chunks[1];
                    } elseif($item['params']=='media="print"'){
                        $chunks=explode('/skin', $this->getSkinUrl($item['name']),2);
                        $lines[$if]['stylesheet_print'][] = "/".$webroot."skin".$chunks[1];
                    }
                    else {
                        $lines[$if]['other'][] = sprintf($stylesheet, $this->getSkinUrl($item['name']), $item['params']);
                    }
                    break;

                case 'rss':
                    $lines[$if]['other'][] = sprintf($alternate, 'application/rss+xml'/*'text/xml' for IE?*/, $item['name'], $item['params']);
                    break;

                case 'link_rel':
                    $lines[$if]['other'][] = sprintf('<link %s href="%s" />', $item['params'], $item['name']);
                    break;

                case 'ext_js':
                default:
                    $lines[$if]['other'][] = sprintf('<script type="text/javascript" src="%s"></script>',$item['name']);
                    break;

            }
        }
        $excludedJs=$this->fetchConfig(Mage::getStoreConfig( self::CFG_ROOT.self::EXJS));
        $exAfterJs=$this->fetchConfig(Mage::getStoreConfig( self::CFG_ROOT.self::EXAFTERJS));
        $excludedCss=array();

        foreach ($lines as $if=>$items) {
            if (!empty($if)) {
                $html .= '<!--[if '.$if.']>'."\n";
            }

            /*if(!stripos($item['name'], "http")===false){
                echo "<br>".;
            }*/
            if (!empty($items['stylesheet'])) {

                $exclude=$this->processExcludes($items['stylesheet'],$stylesheet,$excludedCss, array());
                $html .= implode($exclude['before'],"\n");

                $cssBuild = Mage::getModel('speedster/buildSpeedster')->__construct($items['stylesheet'],BP);

                foreach ($this->getChunkedItems($items['stylesheet'], $baseJsFast.$cssBuild->getLastModified()) as $item) {
                    $html .= sprintf($stylesheet, $item, 'media="all"')."\n";
                }
                $html .= implode($exclude['after'],"\n");
            }
            if (!empty($items['script']['global']) || !empty($items['script']['skin'])) {
                if(!empty($items['script']['global']) && !empty($items['script']['skin'])) {
                    $mergedScriptItems = array_merge($items['script']['global'], $items['script']['skin']);
                } elseif(!empty($items['script']['global']) && empty($items['script']['skin'])) {
                    $mergedScriptItems = $items['script']['global'];
                } else {
                    $mergedScriptItems = $items['script']['skin'];
                }

                $exclude=$this->processExcludes($mergedScriptItems, $script ,$excludedJs, $exAfterJs);
                $html .= implode($exclude['before'],"\n");

                $jsBuild = Mage::getModel('speedster/buildSpeedster')->__construct($mergedScriptItems,BP);

                foreach ($this->getChunkedItems($mergedScriptItems, $baseJsFast.$jsBuild->getLastModified()) as $item) {
                    $html .= sprintf($script, $item, '')."\n";
                }

               $html .= implode($exclude['after'],"\n");

            }
            if (!empty($items['css_direct'])) {
                foreach ($items['css_direct'] as $item) {
                    $html .= sprintf($stylesheet, $item['name'])."\n";
                }
            }
            if (!empty($items['script_direct'])) {
                foreach ($items['script_direct'] as $item) {
                    $html .= sprintf($script, $item['name'],'')."\n";
                }
            }
            if (!empty($items['stylesheet_print'])) {
               $cssBuild = Mage::getModel('speedster/buildSpeedster')->__construct($items['stylesheet_print'],BP);
                foreach ($this->getChunkedItems($items['stylesheet_print'], $baseJsFast.$cssBuild->getLastModified()) as $item) {
                    $html .= sprintf($stylesheet, $item, 'media="print"')."\n";
                }
            }
            if (!empty($items['other'])) {
                $html .= join("\n", $items['other'])."\n";
            }
            if (!empty($if)) {
                $html .= '<![endif]-->'."\n";
            }
        }

        return $html;
    }

    function fetchConfig($cfgTxt){
        $ret=explode("\n", $cfgTxt);
        foreach ($ret as $key => $value) {
            if(stripos($value, "//")<1){
                $ret[$key]="//".trim(substr($value,1));
            }
        }
        return $ret;
    }

    public function getChunkedItems($items, $prefix='', $maxLen=450)
    {
        $combine= Mage::getStoreConfig( self::CFG_ROOT.self::MERGE);
        $noChunkB4=$this->fetchConfig(Mage::getStoreConfig( self::CFG_ROOT.self::NOCHUNKB4));
        $noChunk=$this->fetchConfig(Mage::getStoreConfig( self::CFG_ROOT.self::NOCHUNK));
        $chunks = array();
        $chunk = $prefix;
        $after= array();

        if($combine){
            //if we are combining files ... chunk away
            foreach ($items as $i=>$item) {
                if(in_array($item,$noChunkB4)){
                    array_unshift($chunks,$prefix.substr($item,1));
                    continue;
                }

                if(in_array($item,$noChunk)){
                    $after[]=$prefix.substr($item,1);
                    continue;
                }

                if (strlen($chunk.','.$item)>$maxLen) {
                    $chunks[] = $chunk;
                    $chunk = $prefix;
                }
                //this is the first item
                if ($chunk === $prefix) {
                    $chunk .= substr($item,1); //remove first slash, only needed to create double slash for minify shortcut to document root
                } else {
                    $chunk .= ','. substr($item,1); //remove first slash, only needed to create double slash for minify shortcut to document root
                }
            }
            $chunks[] = $chunk;
        }else{
            foreach ($items as $i=>$item) {
                $chunks[]=$prefix.substr($item,1);
            }
        }

        return array_merge($chunks,$after);
    }

    function processExcludes(&$arr, $script, $b4=array(), $after=array()){
        $ret=array("before"=>array(),"after"=>array());

        $secure = Mage::app()->getStore()->isCurrentlySecure();
        if ($secure) {
            $issecure=str_replace("index.php/","",Mage::getUrl('',array('_secure'=>true)));
            foreach ($arr as $id=>$script_url) {

                if(stripos($script_url, "http")>-1){
                    $ret['before'][]=sprintf($script, $script_url, "")."\n";
                    unset($arr[$id]);
                    if(stripos($script_url, "jquery.min.js")>-1){
                        $ret['before'][]='<script type="text/javascript" src="'.$issecure.'js/jquery/jquery-no-conflict.js" ></script>';
                    }
                }
               if(in_array($script_url,$b4) || stripos($script_url, "prototype.js")>-1){
                    $ret['before'][]=sprintf($script, $issecure.str_replace("//","", $script_url), '')."\n";
                    unset($arr[$id]);
                }
                else if(in_array($script_url,$after)){
                    $ret['after'][]=sprintf($script, $issecure.str_replace("//","", $script_url), '')."\n";
                    unset($arr[$id]);
                }
            }

        }
        else {
            $insecure=str_replace("index.php/","",Mage::getBaseUrl());
            foreach ($arr as $id=>$script_url) {


                if(stripos($script_url, "http")>-1){
                    $ret['before'][]=sprintf($script, $script_url, "")."\n";
                    unset($arr[$id]);
                    if(stripos($script_url, "jquery.min.js")>-1){
                        $ret['before'][]='<script type="text/javascript" src="'.$insecure.'js/jquery/jquery-no-conflict.js" ></script>';
                    }
                }
                if(in_array($script_url,$b4) || stripos($script_url, "prototype.js")>-1){
                    $ret['before'][]=sprintf($script, $insecure.str_replace("//","", $script_url), '')."\n";
                    unset($arr[$id]);
                }
                else if(in_array($script_url,$after)){
                    $ret['after'][]=sprintf($script, $insecure.str_replace("//","", $script_url), '')."\n";
                    unset($arr[$id]);
                }
            }

        }



        return $ret;
    }


}
